package com.example.bhavik.restaurant;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

public class SelectRestaurants extends AppCompatActivity {

    Spinner dropdown;
    ArrayAdapter<CharSequence> staticDropdown;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_restaurants);

        dropdown = (Spinner) findViewById(R.id.spinnerDropDown);
        // Create an ArrayAdapter using the string array and a default spinner
        staticDropdown = ArrayAdapter.createFromResource(this, R.array.select_type, android.R.layout.simple_spinner_item);

        // Specify the layout to use when the list of choices appears
        staticDropdown.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // Apply the adapter to the spinner
        dropdown.setAdapter(staticDropdown);
    }
}
